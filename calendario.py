#!/usr/bin/python3

calendar = {}


def add_activity(date, time, activity):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity


def get_time(atime):
    """Devuelve todas las actividades para
     la hora atime como una lista de tuplas"""
    activities = []
    for date, times in calendar.items():
        for t, activity in times.items():
            if t == atime:
                activities.append((date, t, activity))
    return sorted(activities)


def get_all():
    """Devuelve todas las actividades en el
     calendario como una lista de tuplas"""
    all_activities = []
    for date, times in sorted(calendar.items()):
        for time, activity in sorted(times.items()):
            all_activities.append((date, time, activity))
    return sorted(all_activities)


def get_busiest():
    """Devuelve la fecha con más actividades, y su número de actividades"""
    busiest_date = max(calendar, key=lambda x: len(calendar[x]))
    busiest_activities = calendar[busiest_date]
    busiest_no = len(busiest_activities)
    return (busiest_date, busiest_no)


def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")


def check_date(date):
    """Comprueba si una fecha tiene el formato correcto (aaaa-mm-dd)
    Devuelve True o False"""
    date_parts = date.split('-')
    if len(date_parts) == 3:
        year, month, day = date_parts
        if year.isdigit() and month.isdigit() and day.isdigit():
            return 0 <= int(year) <= 2100 and 1 <= int(
                month) <= 12 and 1 <= int(day) <= 31
    return False


def check_time(time):
    """Comprueba si una hora tiene el formato correcto (hh:mm)
    Devuelve True o False"""
    time_parts = time.split(':')
    if len(time_parts) == 2:
        hour, minute = time_parts
        if hour.isdigit() and minute.isdigit():
            return 0 <= int(hour) <= 23 and 0 <= int(minute) <= 59
    return False


def get_activity():
    """"Pide al usuario una actividad
    En caso de error al introducirla, vuelve a pedirla desde el principio"""
    while True:
        date = input("Fecha (aaaa-mm-dd): ")
        if not check_date(date):
            print("Formato de fecha incorrecto. Inténtalo de nuevo.")
            continue

        time = input("Hora (hh:mm): ")
        if not check_time(time):
            print("Formato de hora incorrecto. Inténtalo de nuevo.")
            continue

        activity = input("Actividad: ")
        return date, time, activity


def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")

    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option


def run_option(option):
    """Ejecuta el código necesario para la opción dada"""
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
    elif option == 'B':
        show(get_all())
    elif option == 'C':
        busiest_date, busiest_no = get_busiest()
        print(f"El día más ocupado es el 
              {busiest_date}, con {busiest_no} actividad(es).")
    elif option == 'D':
        time = input("Hora (hh:mm): ")
        show(get_time(time))


def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)


if __name__ == "__main__":
    main()
